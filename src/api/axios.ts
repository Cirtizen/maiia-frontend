import axios from 'axios';

const config = {
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json' 
    }
}

export function populateDatabase(): Promise<void> {
    return axios.put("http://localhost:8080/post", {}, config);
}

export function getPosts() : Promise<any> {
    return axios.get("http://localhost:8080/post");
}