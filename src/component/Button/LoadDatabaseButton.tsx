import React from "react";
import Button from '@material-ui/core/Button';
import { PostDto } from "../../api/dto";
import { getPosts, populateDatabase } from "../../api/axios";

export function LoadDatabaseButton({setPosts}:{setPosts:(posts: PostDto[]) => void}) {

    return <Button variant="contained" onClick={loadDatabase}>Charger la base de données</Button>

    function loadDatabase() {
        populateDatabase().then(_ => {
            getPosts().then(result => {
                setPosts(result.data); 
            })
        })
    }
}