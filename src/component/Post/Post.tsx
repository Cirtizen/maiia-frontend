import React from 'react';
import { PostDto } from '../../api/dto';
import css from "./Post.module.css";

export function Post({postDto}: {postDto: PostDto}) {
    return (
        <div className={css.post}>
            <span>{postDto.title}</span>
            <span> : </span>
            <span>{postDto.body}</span>
        </div>
    );
}