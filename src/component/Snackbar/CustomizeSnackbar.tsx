import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from '@material-ui/lab/Alert';

interface Props {
    severity: string;
    content: string;
}

export function CustomizeSnackbar({severity, content}: Props) {
    return (
        <Snackbar open={true} autoHideDuration={5000} >
            <Alert severity={severity}>{content}</Alert>
        </Snackbar>
    );

    function Alert(props:any) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }
}

