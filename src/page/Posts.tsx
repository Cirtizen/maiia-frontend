import React, { useState, useEffect } from 'react'
import { PostDto } from "../api/dto";
import { LoadDatabaseButton } from "../component/Button/LoadDatabaseButton";
import { Post } from '../component/Post/Post';
import { getPosts } from '../api/axios';

export function Posts() {
    const [posts, setPosts] = useState<PostDto[]>([]);

    useEffect(() => {
        getPosts().then(result => {
            setPosts(result.data); 
        })
    }, [])

    return (
        <div>
           <LoadDatabaseButton setPosts={setPosts} />
           {posts.map((post, idx) => <Post key={idx} postDto={post} />)}
        </div>
    )
}